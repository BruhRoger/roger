function login(username, password)
{
	return (username === 'admin' && password === '1337');
}

$(document).ready(function () {
	const	field_username = $('#field_username');
	const	field_password = $('#field_password');
	$('#btn_login').click(function () {
		if (login(field_username.val(), field_password.val()) === true)
			window.location.href = 'https://www.1337.ma';
		else
			alert('Username and/or password are wrong!');
	});
});
